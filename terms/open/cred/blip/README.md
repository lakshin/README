---
Title:   'Base Linux Proficiency'
Name:    'BLiP'
UUID:    'rwx.gg/blip'
Version: 'v0.0.2'

Description: |
  *Base Linux Proficiency* (BLiP) outlines dependent skills, knowledge, and abilities required before pursuing further learning to master any of the following high-growth technical occupations. Requirements apply to *all* occupations in the list.

  * Senior Software Developer
  * Cybersecurity Professional
  * Full-Stack Web Developer 
  * Site Reliability Engineer
  * IoT Embedded Device Developer

Requirements:

-R: REDO ALL OF THIS BASED ON NEW SCOPE

- R: Become a Trustworthy Member of Human Society
  - R: Think Globally
    - R: What if Everybody Did it? :)
    - R: What Can I Get Away With? :(
    - R: Build and Seek Trust
  - R: Develop Emotional Intelligence 
    - R: Know and Manage One's Emotions
      - R: Controlling Rage
      - R: Sooth Anxiety
      - R: Don't Worry
      - R: Be Happy
      - R: Control Impluses
      - R: Manage Melancholy
      - R: Self-Motivate
      - R: Remain Patient
      - R: Forgive Yourself
    - R: Recognizing Emotions in Others
      - R: Cultivate Empathy
      - R: Identify and Avoid Triggers
      - R: Inform and Forgive
    - R: Handle Relationships
      - R: Dude, Be Nice
      - R: Respect Muggles, Don't Make Anyone Feel Small
      - R: Share Knowledge Responsibly
        - R: Share, Don't Hoard
        - R: Be Equally Open About Success and Failure
        - R: Have Strong, Well-Researched Opinions
        - R: Hold Opinions Loosely, Be Open to Change
        - R: Explain Opinions, Don't Start with Conclusions
        - R: Accept and Give Useful Criticism 
        - R: Smile, Lauch, Don't Take Yourself Too Seriously

- R: Working from Anywhere

- R: Become an Independent Learner (Autodidact)
  - R: Do Your Own Research
  - R: Think Critically
  - R: Identifying Quality Information
  - R: Challenge Yourself
    - R: Practice Your Own Exercises
    - R: Create Your Own Projects 
    - R: Perform Self-Assessment
    - R: Learn from Failures

- R: Perform Basic Academic Skills
  - R: Communicate
    - R: Read, Write, Speak Native Language Proficiently
    - R: Read, Write, Speak English Proficiently
    - R: Type 30+ WPM
  - R: Perform Basic Technical Math
    - R: Perform Basic Math Operations
      - R: Add
      - R: Subtract
      - R: Multiply
      - R: Divide
      - R: Get Remainder (Modulo)
    - R: Use Variables
    - R: Use Functions
    - R: Count and Convert Between Common Bases
      - R: Convert Base2 (Binary)
      - R: Convert Base10 (Decimal)
      - R: Convert Base16 (Hexadecimal)
      - R: Convert Base64
    - R: Perform Bitwise Operations on Bitfields
      - R: Use NOT
      - R: Use AND
      - R: Use OR
      - R: Use XOR
      - R: Shift Left
      - R: Shift Right
      - R: Rotate

- R: Use and Administer Any Common Computer
  - R: Use and Administer Linux (Debian-Based) OS
  - R: Use and Administer Apple Mac OS
  - R: Use and Administer Microsoft Windows OS

- R: Use Common Mobile Devices

- R: Manage Personal Knowledge and Data
    1. Write Simplified Pandoc Markdown
    1. Code Structured Data
        1. Code JSON
        1. Code YAML
        1. Code TOML (INI)
        1. Code XML
        1. Code CSV and Delimited
    1. Code Basic HTML
    1. Code Basic CSS
    1. Capture and Edit Simple Images and Diagrams
        1. Take a Regional Screenshot
        1. Describe Raster and Vector Formats
        1. Use Inkscape
        1. Use Glimpse
        1. Use Draw.io
        1. Use Krita

1. Use Basic Tools and Utilities
    1. Use Desktop Environment
    1. Manage Passwords Securely
    1. Use Chromium-Based Web Browser
    1. Expand Compressed Files
    1. Use Terminal Interface 
        1. Use Basic Commands
        1. Use Secure Shell

1. Use Developer Tools 
    1. Edit Text and Code
        1. Use VSCode (GUI)
        1. Use Nano (TUI)
        1. Use Vim (TUI)
        1. Use GitLab
    1. Source Control
        1. Git
    1. Use Virtualisation Software
    1. Use Docker Containerization
    1. Use Chrome/Chromium DevTools
    1. Use Curl
    1. Use OpenPGP (GPG)

1. Use Essential Services
    1. Communications
        1. Email
        1. Chat Services 
        1. Social Media
        1. Streaming
        1. Newsgroups
        1. Mailing Lists
    1. Hosting
        1. Source
            1. GitHub
            1. GitLab
        1. Web
        1. Domain
        1. Platform
            1. Digital Ocean

1. Develop Basic Applications
    1. Code the Basic 
        1. Modern JavaScript
        1. C
        1. Bash

1. Basic Network Setup and Administration 
    1. VPN
        1. OpenVPN
    1. TCP/IP Model
    1. OSI Model
    1. Router Maintenance

1. Explain How Internet Works

---
            1. Data Structures
            1. Algorithms

## Missions and Goals

Activate those with experience who don't necessarily have *any* academic training.

* Identify them.
* 

## Open Credential Requirements Model

Just a data model for consistently capturing consistently evolving requirements for a given credential.

## Concepts

### The Hacker Mentality

*Hacker* mentality is thinking *outside* the box to get *inside* the box, a state of mind where one is constantly seeking alternative means of accomplishing a task.

"Just cuz it works is not enough, learn *why* it works."

"The amount of learning it directly proportional to the amount of broken stuff around."

### Focusing on Solutions

Learn to solve. Solve to learn.

### Truth Through Questions

Plato thought everyone has the truth in them, we just forgot it (Socractic Method, maieutics).

### The Downsides of Curiosity

"Knowledge is a mile wide and an inch deep"

### How to Motivate?

1. "Fill the bucket"
1. "Light a fire."
1. "Fan a flame." (preferred)

"What do you do if there's no flame?"

"*Everyone* has a fire, you just have to find it."

"The flame is desire itself. If there is no desire there is no desire even to eat."

"People don't spend time trying to figure out their flame."

"Trust."

### How to Set Boundaries without Damaging Trust



# FAQ

## Questions

* What do we use to determine typing speed?
* What are the specifics for all this?

## Typing Resources

* <https://nitrotype.com>
* <https://typing.com>
* <https://zty.pe>
* <http://www.mavisbeaconfree.com/>



Resources:
  Books:
    - Name: The Tao of TMUX
      URL:  https://leanpub.com/the-tao-of-tmux/read 
    - Name: Learn PowerShell in a Month of Lunches
      URL:  <https://www.manning.com/books/learn-windows-powershell-in-a-month-of-lunches-third-edition>
    - Name: Art-Learning-Journey-Optimal-Performance
      URL:  https://www.amazon.com/Art-Learning-Journey-Optimal-Performance/dp/0743277465
    - Name: Drive
      URL:  ?? 
---
