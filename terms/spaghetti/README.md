---
Title: What is Spaghetti Code?
Subtitle: Mass of Confusing Code
tpl-h1duck: true
---

> "I just need to add this one thing for now."  

*Spaghetti code* is [technical debt](https://duck.com/lite?kae=t&q=technical debt) that has accrued within a [code base](https://duck.com/lite?q=code base) from [procrastinating](https://duck.com/lite?q=procrastination) proper [refactoring](https://duck.com/lite?q=refactoring code) and testing.

## Spaghetti Code Kills

In the case of Toyota, a NASA engineer demonstrated that "spaghetti code" had killed those whose accelerator pedals got stuck.

