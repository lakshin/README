---
Title: What is a Task?
Subtitle: Something Requiring a Skill to Execute
tpl-h1duck: true
---

A *task* is the specification of something that must be accomplished. It can be very detailed or described simply. Tasks begin with an imperative action verb and usually have a target object. 

:::co-fyi
Sometimes an explanation or discussion related to a task uses a *gerund* form of the verb ending in `-ing`. This is common in publications but less common on job descriptions. Therefore, it is recommended that use of gerunds be avoided when writing about tasks.
:::


