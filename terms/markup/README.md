---
Title: Markup Language
Subtitle: Combining Writing with Tags Marking Meaning and Style
tpl-h1duck: true
---

Markup is writing that is marked up with formatting symbols and syntax. [HTML](/lang/html/) and [Markdown](/lang/md/) are examples of markup languages.
