---
Title: What is Infrastructure as a Service / IaaS?
Subtitle: Providing Server Hosting from the Cloud
tpl-h1duck: true
---

*IaaS* is [infrastructure](/terms/infra/) provided as a service allowing it to be turned up or down depending on varying needs at different times. Usually this is what people mean when they say "cloud" services (although that is [not all](../)). [Amazon (AWS)](), [Microsoft (Azure)](), [Google (Compute Engine)](), [Digital Ocean](), and [Linode]() are all examples of IaaS providers.

## See Also

* [Cloud Services](../)
