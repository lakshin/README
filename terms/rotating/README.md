---
Title: What is a Rotating Release?
Subtitle: Ongoing Releases Rather Than in Batches
tpl-h1duck: true
---

A *rotating release* is the process of releasing new content or code on an ongoing, rotating cycle rather than waiting for a bunch of changes and releasing them all at once. This approach provides the most up-to-date content and code and offsets the risk of releasing bugs with the ability to instantly release again to correct them if and when [continuous integration testing](/terms/cicd/) has failed.
