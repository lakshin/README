---
Title: What Does Ubiquitous Mean?
Subtitle: It's Just Everywhere
tpl-h1duck: true
---

*Ubiquitous* means something is just everywhere, so much so that it is assumed to be there and sometimes not even really noticed any longer. Here's some examples of things that are ubiquitous:

* Mobile phones
* Cars
* Roads
* Computers
* Web browsers
* JavaScript
* Linux operating system
* Vi editor

