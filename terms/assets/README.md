---
Title: What are Assets?
Subtitle: Anything of Value
tpl-h1duck: true
---

An *asset* is anything of value but in tech it generally means files that are used elsewhere --- usually images, audio, video, 3D models, documents, fonts and such. Assets are distinct from your code. They are resources your code loads and controls in your application.

It is common to have an `/assets/` directory to hold all your web site stuff away from all the informational content --- especially when creating a [knowledge base](/knowledge/base/) such as this one.
