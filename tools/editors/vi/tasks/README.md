---
Title: Vi/m Tasks
tpl-h1duck: true
---

Command Mode

* Move Down One Line
* Change a Single Word
* Save and Exit

Insert Mode

* Move Down One Line in Insert Mode

