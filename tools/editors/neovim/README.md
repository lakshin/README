---
Title: To NeoVim, Or Not to NeoVim
Subtitle: An Honest Question
tpl-h1duck: true
---

NeoVim or `nvim` is a popular replacement to the classic [Vim](../vim/).

## Pros of Using NeoVim

### Multiple Language Plugin Support

TODO flesh out better pro description.

Vim has this as well.

### Subshell for Plugins 

TODO flesh out better pro description.

This is for people who don't know how to use the existing [Vi Wands](../vi/#magic)

### Just Better Defaults

### Just a Better Out of the Box

* More innovation
* Better internals
* Better team
* Better motivation to submit improvements
* Better code base

### External Plugins in Separate Process

### Better Support for Alacritty

### Gives Vim Healthy Competition

### It Has Panes

## Cons

### Bugs? Stability?

## See Also

* <https://neovim.io/doc/user/vim_diff.html>
