---
Title: Free Software Foundation
Subtitle: Slowly Descending Into Madness
tpl-h1duck: true
---

The Free Software Foundation, headquartered in Boston, was started by Richard Stallman in 1985 and maintains the [copyleft licenses](https://duck.com/lite?kae=t&q=copyleft licenses) of the [GNU project](https://duck.com/lite?kae=t&q=GNU project). The FSF was sharply criticized by [Linus Torvaldz](https://duck.com/lite?kae=t&q=Linus Torvaldz) who stated that he "doesn't want to have anything to do with the FSF ever again" after their handling of the [GPLv3 license](https://duck.com/lite?q=GPLv3 license) roll out.

[Stallman has since been removed from any leadership roll in in FSF but continues to lead the GNU project.](https://duck.com/lite?kae=t&q=Stallman has since been removed from any leadership roll in in FSF but continues to lead the GNU project.)

## See Also

* [Linus Explains Why He's Against GPLv3 and FSF](https://youtu.be/PaKIZ7gJlRU)

