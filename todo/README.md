---
Title: TODO List
Subtitle: RWX Stuff That Needs Work
---

Here are the upcoming planned changes and new content to RWX.GG. This TODO list is maintained instead of an issues system on GitLab only because it is far easier to maintain and regularly access. As priority of TODO items changes they are elevated to the top. Past items are removed entirely or transferred to the [Changes](/changes/) overview.

1. Get rid of `magic` in `magic wand` references cuz *some people* think nsfw.

1. `co-rage` -> `co-mad` and check all the others

1. Change everything to singular.

1. Test on Android device.

1. Review all for consistent use of FAQ styling

1. Write an `audit` script that combines [check](/check) with validation of consistent use of [callouts](/contrib/styleguide/) and any other style guide violations that might creep in. Have check only run on files that have changed since the last run of `check`.

1. Add *spoiler* mode that will automatically hide all the code rather than give away the commands so that it can be used to review as you go.

1. Check all `sh` code blocks to ensure they shouldn't be `bash` instead. Check `pandoc --list-highlight-languages` to be sure of others.

1. Read global knowledge base data from `/README.md` YAML

## Long Term

1. Think about scenario where nodes are moved and how would affect README.world subscribers if at all without `_redirect`.

1. Need a *full* Git primer in the early stages of the boost.

1. Replace [TLCL](/books/tlcl/) with *Linux Terminal Mastery*, a knowledge app that can double as a book, PDF, or whatever. The world needs a *true* terminal mastery book that weaves together deep knowledge and skill in using the fastest human-computer interface physically possible. The rather shallow and inexperienced "Keyboard Tricks" chapter in TLCL was really the trigger. It is disastrously bad and uninformed.
