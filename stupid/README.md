---
Title: Stupid Bad Advice
Subtitle: Just Don't Do These Things Uninformed People Will Tell You
---

There is so much absolutely horrible advice out there that it has become necessary to create this index of it so you can just send people to it instead of fighting over it. Usually people telling you to do this stuff are just trying to help you but are uninformed. *These people aren't stupid*, but their advice definitely is. Usually once they know how bad it is they will correct themselves. If not, well, then yeah they probably are stupid because they care more about their ego and being right than good information. At least you will know.

* [Don't Pick Emacs Just Cuz](./emacs/)
* [Don't Ever Use Zsh](./zsh/)
* [Don't Use Control L to Clear the Screen](./controll/)
* [Don't Cat to a Shell Pipe](./catpipe/)
* [Don't Pipe to a Shell While Loop](./pipe2while/)
* [Don't Use and Promote App Stores](./appstores/)

:::co-fyi
Don't be confused by the titles. It's easier to have the title *counter* the bad advice rather than state it. So it's not a double-negative ("don't don't").
:::
