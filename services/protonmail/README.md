---
Title: ProtonMail Secure Email Service
Subtitle: World's Best Email Provider
tpl-h1duck: true
---

[ProtonMail](https://protonmail.com) is currently the world's leading provider of truly email that is both private and secure. It is an [essential tool](/essentials/) allowing for more secure creation of other accounts on other [services](/services/).

:::co-faq

## What about Tutanota?

Besides not being able to spell it correctly two times in a row, Tutanota 

## Why not just make society transparent?

> It's good to have a king, when the king is good.

Transparency has a lot of advantages when those in power aren't evil, but when is the last time that those in power *weren't* evil in some way.

:::

## See Also

* [David Brin's idea of the Transparent Society](https://duck.com/lite?q=David Brin's idea of the Transparent Society)
* [Lavabit](https://duck.com/lite?q=Lavabit)

