---
Title: GitLab Account Setup
Subtitle: Git Hosting on GitLab FTW
tpl-h1duck: true
---

GitLab is currently the [best](../isbest/) [Git](https://duck.com/lite?q=Git) hosting provider. Setting up an account on GitLab is easy.

