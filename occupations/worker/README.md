---
Title:   'Modern Work Skills'
Name:    'BLiP'
UUID:    'rwx.gg/worker'
Version: 'v0.0.1'

Description: |
  Skills for the modern work environment.

Requirements:

- R: FINISH REFACTORING AND CLEAN UP

- R: Become a Trustworthy Member of Human Society
  - R: Think Globally
    - R: What if Everybody Did it? :)
    - R: What Can I Get Away With? :(
    - R: Build and Seek Trust
  - R: Develop Emotional Intelligence 
    - R: Know and Manage One's Emotions
      - R: Controlling Rage
      - R: Sooth Anxiety
      - R: Don't Worry
      - R: Be Happy
      - R: Control Impluses
      - R: Manage Melancholy
      - R: Self-Motivate
      - R: Remain Patient
      - R: Forgive Yourself
    - R: Recognizing Emotions in Others
      - R: Cultivate Empathy
      - R: Identify and Avoid Triggers
      - R: Inform and Forgive
    - R: Handle Relationships
      - R: Dude, Be Nice
      - R: Respect Muggles, Don't Make Anyone Feel Small
      - R: Share Knowledge Responsibly
        - R: Share, Don't Hoard
        - R: Be Equally Open About Success and Failure
        - R: Have Strong, Well-Researched Opinions
        - R: Hold Opinions Loosely, Be Open to Change
        - R: Explain Opinions, Don't Start with Conclusions
        - R: Accept and Give Useful Criticism 
        - R: Smile, Lauch, Don't Take Yourself Too Seriously

- R: Working from Anywhere
- R: Know Your Value
    - R: Self Evaluation
    - R: Imposter Syndrome
    - R: Dunning Kreuger

- R: Become an Independent Learner (Autodidact)
  - R: Do Your Own Research
  - R: Think Critically
  - R: Identifying Quality Information
  - R: Challenge Yourself
    - R: Practice Your Own Exercises
    - R: Create Your Own Projects 
    - R: Perform Self-Assessment
    - R: Learn from Failures

- R: Perform Basic Academic Skills
  - R: Communicate
    - R: Read, Write, Speak Native Language Proficiently
    - R: Read, Write, Speak English Proficiently
    - R: Type 30+ WPM
  - R: Perform Basic Technical Math
    - R: Perform Basic Math Operations
      - R: Add
      - R: Subtract
      - R: Multiply
      - R: Divide
      - R: Get Remainder (Modulo)
    - R: Use Variables
    - R: Use Functions
    - R: Count and Convert Between Common Bases
      - R: Convert Base2 (Binary)
      - R: Convert Base10 (Decimal)
      - R: Convert Base16 (Hexadecimal)
      - R: Convert Base64
    - R: Perform Bitwise Operations on Bitfields
      - R: Use NOT
      - R: Use AND
      - R: Use OR
      - R: Use XOR
      - R: Shift Left
      - R: Shift Right
      - R: Rotate

- R: Use and Administer Any Common Computer
  - R: Use and Administer Linux (Debian-Based) OS
  - R: Use and Administer Apple Mac OS
  - R: Use and Administer Microsoft Windows OS

- R: Use Common Mobile Devices

- R: Manage Personal Knowledge and Data
  - R: Write Simplified Pandoc Markdown
  - R: Code Structured Data
      - R: Code JSON
      - R: Code YAML
      - R: Code TOML (INI)
      - R: Code XML
      - R: Code CSV and Delimited
  - R: Code Basic HTML
  - R: Code Basic CSS
  - R: Capture and Edit Simple Images and Diagrams
      - R: Take a Regional Screenshot
      - R: Describe Raster and Vector Formats
      - R: Use Inkscape
      - R: Use Glimpse
      - R: Use Draw.io
      - R: Use Krita

- R: Use Basic Tools and Utilities
    - R: Use Desktop Environment
    - R: Manage Passwords Securely
    - R: Use Chromium-Based Web Browser
    - R: Expand Compressed Files
    - R: Use Terminal Interface 
        - R: Use Basic Commands
        - R: Use Secure Shell

- R: Use Developer Tools 
    - R: Edit Text and Code
        - R: Use VSCode (GUI)
        - R: Use Nano (TUI)
        - R: Use Vim (TUI)
        - R: Use GitLab
    - R: Source Control
        - R: Git
    - R: Use Virtualisation Software
    - R: Use Docker Containerization
    - R: Use Chrome/Chromium DevTools
    - R: Use Curl
    - R: Use OpenPGP (GPG)

- R: Use Essential Services
    - R: Communications
        - R: Email
        - R: Chat Services 
        - R: Social Media
        - R: Streaming
        - R: Newsgroups
        - R: Mailing Lists
    - R: Hosting
        - R: Source
            - R: GitHub
            - R: GitLab
        - R: Web
        - R: Domain
        - R: Platform
            - R: Digital Ocean

- R: Develop Basic Applications
    - R: Code the Basic 
        - R: Modern JavaScript
        - R: C
        - R: Bash

- R: Basic Network Setup and Administration 
    - R: VPN
        - R: OpenVPN
    - R: TCP/IP Model
    - R: OSI Model
    - R: Router Maintenance

- R: Explain How Internet Works

---
            - R: Data Structures
            - R: Algorithms

## Missions and Goals

Activate those with experience who don't necessarily have *any* academic training.

* Identify them.
* 

## Open Credential Requirements Model

Just a data model for consistently capturing consistently evolving requirements for a given credential.

## Concepts

### The Hacker Mentality

*Hacker* mentality is thinking *outside* the box to get *inside* the box, a state of mind where one is constantly seeking alternative means of accomplishing a task.

"Just cuz it works is not enough, learn *why* it works."

"The amount of learning it directly proportional to the amount of broken stuff around."

### Focusing on Solutions

Learn to solve. Solve to learn.

### Truth Through Questions

Plato thought everyone has the truth in them, we just forgot it (Socractic Method, maieutics).

### The Downsides of Curiosity

"Knowledge is a mile wide and an inch deep"

### How to Motivate?

- R: "Fill the bucket"
- R: "Light a fire."
- R: "Fan a flame." (preferred)

"What do you do if there's no flame?"

"*Everyone* has a fire, you just have to find it."

"The flame is desire itself. If there is no desire there is no desire even to eat."

"People don't spend time trying to figure out their flame."

"Trust."

### How to Set Boundaries without Damaging Trust



# FAQ

## Questions

* What do we use to determine typing speed?
* What are the specifics for all this?

## Typing Resources

* <https://nitrotype.com>
* <https://typing.com>
* <https://zty.pe>
* <http://www.mavisbeaconfree.com/>



Resources:
  Books:
    - Name: The Tao of TMUX
      URL:  https://leanpub.com/the-tao-of-tmux/read 
    - Name: Learn PowerShell in a Month of Lunches
      URL:  <https://www.manning.com/books/learn-windows-powershell-in-a-month-of-lunches-third-edition>
    - Name: Art-Learning-Journey-Optimal-Performance
      URL:  https://www.amazon.com/Art-Learning-Journey-Optimal-Performance/dp/0743277465
    - Name: Drive
      URL:  ?? 
---
