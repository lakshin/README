---
Title: Bash Language Tasks
Subtitle: HowTo, Recipies, Tutorials
tpl-h1duck: true
---

This is a comprehensive list of Bash [programming](/terms/programming/) [tasks](/terms/tasks/) organized by how commonly you might be asked to use them and therefore importance. Master them progressively in order and [read](/terms/rwx/r/) about the terms and concepts related to them. [Write](/terms/rwx/w/) your own [codebook](/terms/codebook/) containing code with personal notes as you go. Make sure to [exercise](/terms/rwx/x/) your new skills as soon as you learn them by completing *and understanding* some of [these mini projects](../mmp/) or others of your own creation.

:::co-stop
All the listed tasks could have "from the terminal command line" added to the end. This is assumed.
:::

* Create a Bash Script
    * Create a New File
    * Turn a File Into a Bash Script 

*This list of tasks is obviously not yet complete but will eventually be. Adding new tasks will be an ongoing effort. Perhaps you would like to [help](/contrib/)?*
