---
Title: Bash Scripting Language
Subtitle: The Default Interactive Linux Shell
tpl-h1duck: true
---

*Bourne Again Shell* is default interactive [user](/terms/users/) [shell](/terms/shell/) of the Linux operating system. It is also a powerful [scripting language](/lang/) known for creating useful scripts complex single-line commands quickly. As such it has a rather unusual [syntax](/terms/syntax/) when compared to other languages. Bash usage and programming are a [primary focus](/books/tlcl/) of the [Linux Beginner Boost](/boost/schedule/).

## Bash Shell Survival {#survive}

Whether you are just starting out as a new technologist or have been one for a while but not really learned the terminal command line shell then you might find yourself stuck or not knowing what to do. Here's a minimal survival guide to help you out or get you started. Remember as you master these that there is a *lot* more to come. Don't worry about understanding every, single option with every command at the moment or even why they do what they do. That will come in time. 

:::co-stop
Before you get started make sure you understand all the [keyboard symbols](/typing/#symbols) and just know that you can usually combine two options that use a [dash](/typing/#symbols) like `ls -al`.
:::

:::co-warn
If you happen to know (or have had someone else tell you about) the `ls` aliases like `ll` just don't use them for now. It is far more important for you to learn the actual commands, not the alias shortcuts. Later after you have *really* mastered the *actual* command and options consider adding your own aliases.
:::

### Cozy Shell Commands {#cozy}

First the *cozy* commands, those that make you feel at home and like you know where you are, how to get around, what stuff is, and what's inside.

------------------ ------------------------------------------
 Command            Description
------------------ ------------------------------------------
 `ls`              List *visible* files and directories in 
                   the current directory.

 `ls -a`           List *all* files and directories in 
                   the current directory.

 `ls -l`           List the details of files and directories
                   including permissions.

 `pwd`             Print the working directory (where
                   you are).

 `clear`           Clear the screen. [*Do not use
                   `Control`+`L`!*](/stupid/controll)

 `cd`              Change into the home directory from
                   anywhere.

 `cd ..`           Change parent of current directory. The
                   two dots `..` mean parent.

 `cd -`            Change into the previous directory, which
                   is nice for changing back and forth a lot.


 `cat <file>`      Shows the content of the file.

 `less <file>`     Shows the content of a long file that
                   you can page through with `Space`. 
                   Remember `q` to quit.

 `type <this>`     Check if this command exists, what
                   *type* of command it is (alias, function,
                   binary executable), and where it lives.
------------------ ------------------------------------------

### Existential Shell Commands {#existential}

Now for the *existential* commands that create, change, and delete stuff. You don't need to fear these commands, just be more careful when using them. There is *no* undo on the Linux command line (until you get Git setup).

-------------------------- ------------------------------------------
 Command                   Description
-------------------------- ------------------------------------------
 `file <file>`     Show what kind of file it is.

`mkdir <dir>`              Make a new directory.

`touch <file>`             Create a new empty file. (Or update the
                           time stamp of the file.)

`mv <old> <new>`           Rename a file or directory.

`rm <file>`                Remove a file. 

`rmdir <dir>`              Remove an *empty* directory. (Use `rm`
                           first for the files until it is empty.)

`chmod u+x <file>`         Change a plain text file into an
                           *executable* script command.
                   
`./<script>`               Run a newly made command script in the
                           current directory.
-------------------------- ------------------------------------------

:::co-mad
There is a *reason* dangerous advanced commands like `rm -rf <dir>` and brain-dead things like `Control`-`L` to clear your screen are not included in this survival shell section. It is designed for beginners who might seriously mess themselves up knowing these options exist at this time. Eventually everyone should learn the more advanced options, just not right now. 

By the way, if you do *not* understand why `Control`-`L` is so bad to burn into your muscle memory you simply have not yet learned *basic* command line Linux usage at all. Keep reading and researching. You'll get there.
:::

### Getting Unstuck

Everyone will eventually have their terminal start doing weird things, usually because we have typed something it does not like. It's important that you understand all terminals started out as teletype machines that would actually type stuff out onto paper rather than sending the letters and numbers to a terminal screen for us to read.

#### Interrupting (Stopping) with `Control`+`c`{#interrupt}

Sometimes something will be running and we need to cancel or stop it. Maybe we forgot a quotation mark or wrote an infinite loop and need to interrupt it. To send a running program (called a [process](/terms/process/)) a signal to stop itself (an interrupt) hold down `Control` and tap `c`.

#### Sending *End of File* with `Control`+`d`{#eof}

Sometimes a program wants to see a special *end of file* symbol (which cannot be printed or viewed). To send one of those do `Control`+`d`.

#### Unsuspending with `Control`+`q`{#resume}

Inevitably you will type `Control`+`s` on accident some day. When you do your terminal will completely freeze. That's because you have sent it a *suspend* signal on accident. To quit suspending your terminal causing it to resume output use `Control`+`q` but be careful. What you have typed since the time it was suspended will *still* be printed to the screen and processed. For that reason it is often safer to [interrupt](#interrupt) instead.

:::co-fyi
The annoying suspend stuff is left over from teletype machines that could not keep up with the incoming data to be printed and didn't have buffers large enough to hold it all while the printer caught up. So once upon a time people actually used suspend to slow the output until the teletype printer could catch up.
:::

## Omissions

There are several valuable and important elements of Bash that the book leaves out. Make sure you understand them.

* `select`
* `until`
* `getopts`
* `complete`
* `CDPATH`
* ANSI Color and Terminal Escape Sequences
* Parameter expansion for default values
