---
Title: Course Content Schedule (Next)
Subtitle: What When
---

*This is the revised schedule that will be used for the next boost.*

Content is organized by [weeks](/boost/calendar/), weeks by days, days by [sessions](/boost/times/). Current week is highlighted. Topics and skills are included that are required by *all* [target occupations](/occupations/). As with everything in this [knowledge app](/knowledge/apps/) the schedule and content will change frequently. Check back often.

1. Get Started
    1. Get Oriented
        1. Get Accounts
        1. Create Notes Project Repo
        1. Start Taking Notes
        1. Grok Grok
        1. Grok Course
        1. Learn to Learn
        1. Learn Basic Markdown
        1. Convert Notes to Markdown
        1. Publish First JAMstack Site
    1. Get Me a Linux Terminal, Stat!
        1. Install WSL on Windows
        1. Install PopOS in VirtualBox
        1. Install Ubuntu in Docker
        1. Try REPL.it
        1. Try PicoCTF.com
    1. Begin with Bash
    1. Get By with Vi
    1. Get Good at Git and `dotfiles`

1. 

1. [Grokking, Installing, and Running Linux](#wk02)
1. [The Linux Command Line, I-III](#wk03)
1. [The Linux Command Line, IV Bash Scripts](#wk04){.current-week}
1. Structured Data and Knowledge Source
1. Eloquent JavaScript, Chapters 1-7
1. Eloquent JavaScript, Chapters 8-14
1. Eloquent JavaScript, Chapters 15-21
1. Learning Web Design, I,V HTML/Images
1. Learning Web Design, II CSS
1.  Head First C, Chapters 1-4
1. Head First C, Chapters 5-8
1. Head First C, Chapters 9-12
1. Head First Go, Chapters 1-5
1. Head First Go, Chapters 6-12
1. Head First Go, Chapters 13-16
1. Now What
    1. Get Good to Gig 
    1. Stay Prescient
    1. Make Contact
    1. Follow Your Favorites

## Day 1

## Day 2 
