---
Title: Style Guide
Subtitle: Follow These When Contributing
---

Here's a summary of styles used on `rwx.gg` so that contributors can keep things *extremely* consistent. The value proposition of this knowledge app (above, say, a wiki) is this consistency. Feel free to [open an issue](https://gitlab.com/rwx.gg/README/-/issues) if you feel that anything violates this consistency. Thanks.

## Writing Style, Grammar, and Usage

Except where otherwise noted below, the [RWX.GG]{.spy} project follows [The Gregg Reference Manual](/books/gregg) for all written content.

## General Rules

* Keep main template at `/assets/template.html`

* Keep main style sheet at `/assets/style.css`

* Keep main sheet at `/assets/style.css`

* Add node local template override to `<node>/template.html`

* Keep node local body overrides as `<node>/body.html`

* Keep node local style overrides as `<node>/style.css`

* Never use `H1` (`#`) headings. Use `Title` in YAML instead.

* Do not use [gerunds](/terms/task/) to describe tasks and actions.

* Use questions for all [/terms/](/terms/) to promote hits.

* Use imperative verbs in IDs and instructional steps.

* No parenthetical statements. Use dash, comma or separate sentence.

* Very judicious use of minimal italic.

* Only `README.md` files are rendered. Other markdown ignored.

* Keep nodes composable, don't include too much into one.

* Subnodes of [knowledge nodes](/knowledge/node/) are fine.

* Only use parent (`../`) directory refs when needed.

* Always use site root paths to other nodes (ex: `/node/`)

* Always include trailing slash in node paths even with hash.

* Keep node directory names easy to type and remember:
    * Pick meaningful names
    * 12 lowercase characters max
    * Use abbreviations and acronyms when possible
    * First character must be letter
    * Letters and numbers only
        * No hyphens, dashes, or underscores
        * No emojis

* Use a default empty link populate line in your `.vimrc` to fill in empty links that pull up searches on Duck.com rather than not link at all. Later you can return and provide local content as time allows. This saves those learning on the site from having to type in all the links to search the Web for terms that are likely to need explanation and allows mobile users to simple tap as they go.

* Never depend on color for anything. Keep stuff black-ish and white.

* Do not put links of any kind in any heading.

* Use `tpl-h1duck: true` to activate link from `h1` heading to DuckDuckGo search.

```vimscript
autocmd vimleavepre *.md !perl -p -i -e 's,\[([^\]]+?)\]\(\),[\1](https://duck.com/lite?kae=t&q=\1),g' %
```

## Callout Types

Here are the different callout types that can be used by adding a `co-` to the beginning. Make sure you know what a Pandoc Markdown "Div" is. (There are a lot of examples already in the content.)

 Identifier    Description
------------   -------------------------------------------
 `faq`           Surround level two headers of FAQs
 `fyi`           Informational but not related
 `btw`           Informational and slightly related
 `fun`           Informational, related, and fun
 `yea`           Something to celebrate.
 `hap`           Something happy
 `sad`           Something sad.
 `mad`           Something mad.
 `tip`           High value information, related ProTips
 `chk`           Something specific to check for
 `duh`          So dumb you'd smack your forehead
 `care`          Take care, be careful
 `warn`          A critical warning
 `stop`          Stop and pay attention, achtung

## Special `faq` Divs

In order for frequently asked question text to be included in the main meta data summations that are used for localized searches all FAQ questions must begin with a level-two heading (`##`) and should also be grouped and encapsulated in a div group as follows:

```markdown
:::co-faq

## How should I include FAQs throughout the content?

This is how

## Do I need them to always be second level headings?

Yes. Nothing forces it, but it is a good convention.

## Should I captialize the FAQ heading?

No. It should read and appear like any other sentence.

:::


```

## Special Link Types

The following link types can be added to a link as `{.<type>}` immediately after the link markdown causing the link to have extra formatting and `before` content including an emoji or special character.

 Identifier    Description
------------   -------------------------------------------
 `see`           Notable internal or external link
 `watch`         Direct link to a video to play 
 `download`      A link to a resource that will download

## YAML Headers

YAML headers contain both meta data information as well as display parameters. The headers from the main `/README.md` file can be considered global and are available to every node in the knowledge base. Header identifiers can literally be any valid YAML but the following are standard and expected when creating the `meta.json` file.

--------------------------------------------------------
 Identifier     Description         
--------------- ----------------------------------------
 `Title`        Becomes the title of the entire
                knowledge base. If not set will be set
                to `Short` if available. Overriden by
                nodes but always applies to whole base.

 `Short`        Short version of the title included in
                every page and usually combined with
                the node's own title.
--------------------------------------------------------

Table Standard YAML Headers

### Use of Case

Use initial caps for headers that are exportable and have relevance. Only initial caps will be added to the `meta.json` file.

Lowercase should be used for localized data usually considered arguments and configuration of the template itself. Use the `tpl-` prefix and whitespace to group these type of headers.

### Knowledge Nodes

