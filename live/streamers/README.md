---
Title: Other Related Streamers
Subtitle: More Than One Way to Tune In
---

Even though [rwxrob.live](https://rwxrob.live) is the primary stream --- for now --- here are some other streamers you should follow who regularly stream about related topics. Some are experienced veterans, others are just working through the course themselves. All of them stream on Twitch. 

:::co-fyi
To add yourself [submit a ticket](https://gitlab.com/rwx.gg/README/-/issues) (or better yet a merge request) with your request to be added.
:::

  Twitch ID         Description
------------------  ------------------------------------------
rwxrob               Currently primary course stream
ltn_bob              Network engineering instructor
zerostheory          Moderator and course participant
iScreaMan23          Moderator and course participant

<script>
<!-- make Twitch IDs clickable -->
let tds=document.querySelectorAll("td")
for (let td of tds) {
  if (td.cellIndex === 0) {
    let id = td.innerHTML
    td.innerHTML = `<a href="https://twitch.tv/${id}">${id}</a>`
  }
}
</script>
